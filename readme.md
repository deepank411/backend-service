# graphql-gateway

### Prepacked With

- All ES6 features(_Arrows, Generators, Spread, etc_).
- Async Functions.
- Static class properties and methods.
- Dynamic Imports.

### Getting Started

- `npm install -g api-doc-generator` to install doc generator.
- `yarn install` to install dependencies.
- Add all your constans to `constants.js` file in the root directory.
- `yarn start` to start development build with nodemon.
- `yarn build` to make production build.
- `yarn start:prod` to start production server.
- `yarn gen` to generate documentation.
