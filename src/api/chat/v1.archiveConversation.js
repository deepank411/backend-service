/* eslint-disable no-underscore-dangle */
import Conversation from '../../models/conversation.model';
import { findOneAndUpdateQuery } from '../../helpers/generalQueries';
import { mongoQueryBuilder, MongoIdType } from '../../helpers/mongoHelpers';
import getConversationWithUserDetails from './v1.getConversationWithUserDetails';

const archiveConversation = async (params) => {
	const paramsToUpdate = params.isArchived
		? {
			$pull: { isArchivedBy: { userId: MongoIdType(params.userId) } },
		}
		: {
			$push: { isArchivedBy: { userId: MongoIdType(params.userId) } },
		};

	const res = await findOneAndUpdateQuery(
		Conversation,
		{
			_id: params.id,
			users: mongoQueryBuilder('elemMatch', {
				userId: MongoIdType(params.userId),
			}),
		},
		paramsToUpdate,
		{ new: true },
	);

	if (!res) {
		throw new Error('Failed to Archive Conversation'); // Either  because conversation didn't exist or because user didn't have permission
	}

	return getConversationWithUserDetails({ conversationId: params.id });
};

export default archiveConversation;
