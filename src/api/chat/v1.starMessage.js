import Message from '../../models/message.model';
import { cleanMongoObject } from '../../helpers/commonHelpers';
import { findOneAndUpdateQuery } from '../../helpers/generalQueries';
import { MongoIdType } from '../../helpers/mongoHelpers';

const starMessage = async (params) => {
	if (!params.id || !params.userId) {
		throw new Error('Incorred payload. Message Id and UserId must be passed to star a message');
	}
	const paramsToUpdate = params.isMessageStarred ? { $pull: { starredBy: { userId: MongoIdType(params.userId) } } } : { $push: { starredBy: { userId: MongoIdType(params.userId) } } };

	const res = await findOneAndUpdateQuery(Message, { _id: params.id }, paramsToUpdate, { new: true });

	if (!res) return null;
	return cleanMongoObject(res.toObject());
};

export default starMessage;
