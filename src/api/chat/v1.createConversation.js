/* eslint-disable no-underscore-dangle */
import Conversation from '../../models/conversation.model';
import Message from '../../models/message.model';
import { saveQuery, aggregateQuery, createMatch, createLookup, createSort, findOneAndUpdateQuery, findOneQuery, findQuery, countQuery } from '../../helpers/generalQueries';
import { cleanMongoObject, isNotEmptyObject, isNotEmptyArray } from '../../helpers/commonHelpers';
import { mongoQueryBuilder, MongoIdType } from '../../helpers/mongoHelpers';
import getConversationWithUserDetails from './v1.getConversationWithUserDetails';

// TODO: improve this function and queries - DKA
const createConversation = async (params) => {
	let isNewConversation = false;
	const conversationUsers = [params.userId, ...params.msgTo].map((u) => ({ userId: MongoIdType(u) }));
	const searchQ = {
		...(params.conversationId && { _id: params.conversationId }),
		$and: [{ users: { $all: conversationUsers, $size: conversationUsers.length } }],
	};
	let conversationData = await findOneQuery(Conversation, searchQ);
	if (!isNotEmptyObject(conversationData)) {
		isNewConversation = true;

		const newConv = new Conversation({
			users: [{ userId: params.userId }, ...params.msgTo.map((x) => ({ userId: x }))],
			...(params.conversationName && { conversationName: params.conversationName }),
			createdBy: params.userId,
		});

		conversationData = await saveQuery(newConv);
		if (!conversationData) {
			throw new Error('could not create conversation');
		}
	}

	// Unarchive the conversation if it was archived by the user
	// await is neccessary because we want to catch the error
	await findOneAndUpdateQuery(
		Conversation,
		{
			_id: conversationData._id,
			isArchivedBy: mongoQueryBuilder('in', [{ userId: MongoIdType(params.userId) }]),
		},
		{
			$pull: { isArchivedBy: { userId: MongoIdType(params.userId) } },
		},
		{ new: true },
	);

	if (params.withUserDetails) {
		return getConversationWithUserDetails({ conversationId: conversationData._id });
	}

	return {
		conversation: cleanMongoObject(conversationData.toObject()),
		isNewConversation,
	};
};

export default createConversation;
