/* eslint-disable no-underscore-dangle */
import Conversation from '../../models/conversation.model';
import Message from '../../models/message.model';
import { saveQuery, aggregateQuery, createMatch, createLookup, createSort, findOneAndUpdateQuery, findOneQuery, findQuery, countQuery } from '../../helpers/generalQueries';
import { cleanMongoObject, isNotEmptyObject, isNotEmptyArray } from '../../helpers/commonHelpers';
import { mongoQueryBuilder, MongoIdType } from '../../helpers/mongoHelpers';

const getConversationWithUserDetails = async ({ conversationId }) => {
	const aggQuery = [
		createMatch({
			_id: MongoIdType(conversationId),
		}),
		createLookup({
			from: 'users',
			localField: 'users.userId',
			foreignField: '_id',
			as: 'userDetails',
		}),
	];
	const data = await aggregateQuery(Conversation, aggQuery);

	const modifiedData = data.map((z) =>
		cleanMongoObject({
			...z,
			...(z.users && { users: z.users.map((y) => cleanMongoObject(y)) }),
			...(z.userDetails && { userDetails: z.userDetails.map((y) => cleanMongoObject(y)) }),
		}),
	);
	return modifiedData[0];
};

export default getConversationWithUserDetails;
