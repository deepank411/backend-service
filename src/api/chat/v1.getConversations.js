/* eslint-disable no-underscore-dangle */
import Conversation from '../../models/conversation.model';
import Message from '../../models/message.model';
import { saveQuery, aggregateQuery, createMatch, createLookup, createSort, findOneAndUpdateQuery, findOneQuery, findQuery, countQuery } from '../../helpers/generalQueries';
import { cleanMongoObject, isNotEmptyObject, isNotEmptyArray } from '../../helpers/commonHelpers';
import { mongoQueryBuilder, MongoIdType } from '../../helpers/mongoHelpers';

const getConversations = async (params) => {
	const aggQuery = [
		createMatch({
			...(params.from && params.to && { lastMessageTime: { $gt: new Date(params.from), $lt: new Date(params.to) } }),
			users: mongoQueryBuilder('elemMatch', {
				userId: MongoIdType(params.userId),
			}),
			// TODO: This will be decided by a query params -> withArchivedConversations
			isArchivedBy: mongoQueryBuilder(params.onlyArchived === 'true' ? 'in' : 'nin', [{ userId: MongoIdType(params.userId) }]),
		}),
		...(params.withUserDetails === 'true'
			? [
				createLookup({
					from: 'users',
					localField: 'users.userId',
					foreignField: '_id',
					as: 'userDetails',
				}),
			]
			: []),
		createSort({
			lastMessageTime: -1,
		}),
	];
	const allConversations = await aggregateQuery(Conversation, aggQuery);

	const unreadCounts = await Promise.all(
		allConversations.map((c) =>
			countQuery(Message, {
				conversationId: c._id,
				userId: mongoQueryBuilder('ne', params.userId),
				'readBy.userId': { $ne: params.userId },
			}),
		),
	);

	const conversationWithUnreadCount = allConversations.map((z, idx) =>
		cleanMongoObject({
			...z,
			unreadCount: unreadCounts[idx],
			...(z.users && { users: z.users.filter((x) => x.userId.toString() !== params.userId).map((y) => cleanMongoObject(y)) }),
			...(z.userDetails && { userDetails: z.userDetails.filter((x) => x._id.toString() !== params.userId).map((y) => cleanMongoObject(y)) }),
		}),
	);
	return conversationWithUnreadCount;
};

export default getConversations;
