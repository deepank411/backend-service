import Message from '../../models/message.model';
import { cleanMongoObject, isNotEmptyArray } from '../../helpers/commonHelpers';
import { updateManyQuery, findQuery } from '../../helpers/generalQueries';
import { mongoQueryBuilder, MongoIdType } from '../../helpers/mongoHelpers';

const readMessage = async (params) => {
	if (!isNotEmptyArray(params.msgIds) || !params.userId) {
		throw new Error('Incorred payload. Message Id and UserId must be passed to read a message');
	}

	const msgIds = params.msgIds.map((id) => MongoIdType(id));

	// NOTE: For now, we have to mark all of the messages for conversation and user if any read message api has been called.
	// because our frontend will reset the unread count as soon as the conversation opens
	const res = await updateManyQuery(
		Message,
		{
			conversationId: params.conversationId,
			userId: mongoQueryBuilder('ne', params.userId),
			readBy: mongoQueryBuilder('nin', [{ userId: MongoIdType(params.userId) }]),
		},
		{ $push: { readBy: [{ userId: MongoIdType(params.userId) }] } },
	);

	const updatedMsgs = await findQuery(Message, { _id: mongoQueryBuilder('in', msgIds) });

	if (!isNotEmptyArray(updatedMsgs)) return null;

	return updatedMsgs.map((m) => cleanMongoObject(m.toObject()));
};

export default readMessage;
