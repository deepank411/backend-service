import Message from '../../models/message.model';
import Conversation from '../../models/conversation.model';
import { cleanMongoObject, ArrayMaybe, isNotEmptyObject, isNotEmptyArray, omitWrapper, nowMongoDate } from '../../helpers/commonHelpers';
import { findOneQuery, findOneAndUpdateQuery, saveQuery, detailedFindQuery, updateManyQuery, findQuery } from '../../helpers/generalQueries';
import { mongoQueryBuilder, newMongoId, MongoIdType } from '../../helpers/mongoHelpers';
import createConversation from './v1.createConversation';
import getConversationWithUserDetails from './v1.getConversationWithUserDetails';

const createMessage = async (params) => {
	const newMessageId = newMongoId();
	const msgTime = nowMongoDate();
	let conversationData;
	if (!params.userId) {
		throw new Error('Sender Id not sent');
	}
	if (!isNotEmptyArray(params.msgTo) && !params.conversationId) {
		throw new Error('Invalid Parameters sent. Please send receipients or conversation id');
	}

	if (params.msgTo.includes(params.userId)) {
		throw new Error('Sender and recipient Ids cannot be same.');
	}

	const recipientIds = params.msgTo.map((r) => ({ userId: MongoIdType(r) }));
	let isConvArchivedByRecipient = false;
	let isNewConversation = false;

	// if conversationId is not passed then call createConversation
	// it will either return the existing conversation between user and recipient or create a new one
	if (!params.conversationId) {
		const dd = await createConversation(params);
		conversationData = dd.conversation;
		isNewConversation = dd.isNewConversation;
	} else {
		isConvArchivedByRecipient = await findOneQuery(Conversation, { _id: params.conversationId, isArchivedBy: mongoQueryBuilder('in', recipientIds) });
	}

	const conversationParams = {
		users: [{ userId: params.userId }, ...ArrayMaybe(params.msgTo).map((x) => ({ userId: x }))],
		lastMessageId: newMessageId,
		lastMessageText: params.msg,
		lastMessageType: params.msgType,
		lastMessageTime: msgTime,
		lastMessageFrom: params.userId,
		isArchivedBy: [],
	};

	conversationData = await findOneAndUpdateQuery(Conversation, { _id: params.conversationId || conversationData.id }, omitWrapper(['users'], conversationParams), { new: true });

	const prop = new Message({
		_id: newMessageId,
		msgType: params.msgType,
		msg: params.msg,
		conversationId: params.conversationId || conversationData.id,
		userId: params.userId,
	});

	const newMsg = await saveQuery(prop);
	if (!newMsg) {
		throw new Error('could not send message');
	}

	conversationData = !params.conversationId || isConvArchivedByRecipient ? await getConversationWithUserDetails({ conversationId: conversationData.id }) : conversationData;

	return {
		message: cleanMongoObject(newMsg.toObject()),
		conversation: isNewConversation ? { ...conversationData, unreadCount: 1 } : conversationData,
		// shouldRecipientGetNotified: !params.conversationId || !!isConvArchivedByRecipient,
		isNewConversation,
	};
};

export default createMessage;
