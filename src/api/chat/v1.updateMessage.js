import Message from '../../models/message.model';
import Conversation from '../../models/conversation.model';
import { cleanMongoObject, ArrayMaybe, isNotEmptyObject, isNotEmptyArray, omitWrapper, nowMongoDate } from '../../helpers/commonHelpers';
import { findOneQuery, findOneAndUpdateQuery, saveQuery, detailedFindQuery, updateManyQuery, findQuery } from '../../helpers/generalQueries';
import { mongoQueryBuilder, newMongoId, MongoIdType } from '../../helpers/mongoHelpers';
import updateConversation from './v1.updateConversation';

const updateMessage = async (params) => {
	const modifiedParams = {
		$push: {
			editHistory: {
				msg: params.oldMsg,
				editedAt: nowMongoDate(),
			},
		},
		msg: params.msg,
	};
	const res = await findOneAndUpdateQuery(Message, { _id: params.id }, modifiedParams, { new: true });
	if (params.isLatestMessage) {
		const convRes = await updateConversation(
			{ _id: params.conversationId, lastMessageId: params.id, lastMessageFrom: params.userId },
			{
				lastMessageTime: nowMongoDate(),
				lastMessageText: params.msg,
			},
		);
		if (convRes === null) {
			throw new Error('Incorrect payload. Please try again.');
		}
	}
	return cleanMongoObject(res.toObject());
};

export default updateMessage;
