import Message from '../../models/message.model';
import { cleanMongoObject } from '../../helpers/commonHelpers';
import { findOneAndUpdateQuery } from '../../helpers/generalQueries';
import { removeContentFromDeletedMessage } from './helpers';
import updateConversation from './v1.updateConversation';

const deleteMessage = async (params) => {
	const res = await findOneAndUpdateQuery(
		Message,
		{ _id: params.id, userId: params.userId },
		{
			isDeleted: 1,
		},
		{ new: true },
	);

	if (res === null) {
		throw new Error('Failed to delete message');
	}
	const d = removeContentFromDeletedMessage(cleanMongoObject(res.toObject()));

	if (params.isLatestMessage === 'true') {
		const convRes = await updateConversation(
			{ _id: params.conversationId },
			{
				lastMessageText: null,
			},
		);
		if (convRes === null) {
			throw new Error('Incorrect payload. Please try again.');
		}
	}
	return d;
};

export default deleteMessage;
