export const removeContentFromDeletedMessage = (msg) => (msg.isDeleted ? { ...msg, msg: null, starredBy: [] } : msg);
