import Message from '../../models/message.model';
import Conversation from '../../models/conversation.model';
import { isNotEmptyObject } from '../../helpers/commonHelpers';
import { findOneQuery, detailedFindQuery } from '../../helpers/generalQueries';
import { mongoQueryBuilder } from '../../helpers/mongoHelpers';
import { removeContentFromDeletedMessage } from './helpers';

const getMessageListing = async (params) => {
	const convData = await findOneQuery(Conversation, { _id: params.conversationId });
	if (isNotEmptyObject(convData)) {
		if (convData.users.findIndex((x) => x.userId.toString() === params.userId) !== -1) {
			const modifiedParams = {
				conversationId: params.conversationId,
				...(params.operator && params.lastMessageId && { _id: mongoQueryBuilder(params.operator, params.lastMessageId) }),
				...(params.from && params.to && { createdAt: { $gt: params.from, $lt: params.to } }),
			};
			const data = await detailedFindQuery(Message, modifiedParams, {
				...('limit' in params && { withLimit: params.limit }),
				...('sortKey' in params &&
					'sortOrder' in params && {
					withSort: {
						[params.sortKey]: params.sortOrder,
					},
				}),
			});
			return data.map(removeContentFromDeletedMessage);
		}
		throw new Error('not authorized.');
	}
	throw new Error('invalid conversation');
};

export default getMessageListing;
