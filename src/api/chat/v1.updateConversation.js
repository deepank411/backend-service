/* eslint-disable no-underscore-dangle */
import Conversation from '../../models/conversation.model';
import Message from '../../models/message.model';
import { saveQuery, aggregateQuery, createMatch, createLookup, createSort, findOneAndUpdateQuery, findOneQuery, findQuery, countQuery } from '../../helpers/generalQueries';
import { cleanMongoObject, isNotEmptyObject, isNotEmptyArray } from '../../helpers/commonHelpers';
import { mongoQueryBuilder, MongoIdType } from '../../helpers/mongoHelpers';

const updateConversation = async (params) => {
	const paramsToUpdate = {
		...(params.conversationName && { conversationName: params.conversationName }),
		...(isNotEmptyArray(params.usersToAdd) && {
			$push: {
				users: {
					$each: params.usersToAdd.map((userId) => ({ userId: MongoIdType(userId) })),
				},
			},
		}),
		...(isNotEmptyArray(params.usersToRemove) && {
			$pull: {
				users: {
					$in: params.usersToRemove.map((userId) => ({ userId: MongoIdType(userId) })),
				},
			},
		}),
	};

	// return conversation if users are already present in conversation
	if (isNotEmptyArray(params.usersToAdd)) {
		const searchQ = {
			_id: MongoIdType(params.conversationId),
			$and: [params.userId, ...params.usersToAdd].map((x) => ({ users: { $elemMatch: { userId: MongoIdType(x) } } })),
		};
		const conversationData = await findOneQuery(Conversation, searchQ);
		if (conversationData) return conversationData.toObject();
	}

	await findOneAndUpdateQuery(Conversation, { _id: params.conversationId }, paramsToUpdate, { new: true, multi: true });
	return getConversationWithUserDetails({ conversationId: params.conversationId });
};

export default updateConversation;
