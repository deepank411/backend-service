/* eslint-disable no-underscore-dangle */
import Conversation from '../../models/conversation.model';
import Message from '../../models/message.model';
import { saveQuery, aggregateQuery, createMatch, createLookup, createSort, findOneAndUpdateQuery, findOneQuery, findQuery, countQuery } from '../../helpers/generalQueries';
import { cleanMongoObject, isNotEmptyObject, isNotEmptyArray } from '../../helpers/commonHelpers';
import { mongoQueryBuilder, MongoIdType } from '../../helpers/mongoHelpers';

const getConversation = async (params) => {
	const conversationUsers = [params.userId, ...params.msgTo].map((u) => ({ userId: MongoIdType(u) }));
	const searchQ = {
		...(params.conversationId && { _id: params.conversationId }),
		$and: [{ users: { $all: conversationUsers, $size: conversationUsers.length } }],
	};

	const data = await findOneQuery(Conversation, searchQ);

	if (!data) return null;

	return cleanMongoObject(data.toObject());
};

export default getConversation;
