import { Router } from 'express';
import { isNotEmptyObject } from '../../helpers/commonHelpers';
import { newMongoId } from '../../helpers/mongoHelpers';

const router = new Router();

router.get('/api/myip', (req, res) => {
	let { ip } = req; // trust proxy sets ip to the remote client (not to the ip of the last reverse proxy server)
	if (ip.substr(0, 7) == '::ffff:') {
		// fix for if you have both ipv4 and ipv6
		ip = ip.substr(7);
	}
	// req.ip and req.protocol are now set to ip and protocol of the client, not the ip and protocol of the reverse proxy server
	// req.headers['x-forwarded-for'] is not changed
	// req.headers['x-forwarded-for'] contains more than 1 forwarder when
	// there are more forwarders between the client and nodejs.
	// Forwarders can also be spoofed by the client, but
	// app.set('trust proxy') selects the correct client ip from the list
	// if the nodejs server is called directly, bypassing the trusted proxies,
	// then 'trust proxy' ignores x-forwarded-for headers and
	// sets req.ip to the remote client ip address

	res.json({ ip });
});

router.route('/api/v1/create-new-mongo-id').get(async (req, res) => {
	if (isNotEmptyObject(req.query)) {
		if (req.query.count == 1) {
			res.create(newMongoId())
				.success()
				.send();
		} else {
			const { count } = req.query;
			res.create(numberRange(1, parseInt(count, 10)).map((x) => newMongoId()))
				.success()
				.send();
		}
	} else {
		res.create(newMongoId())
			.success()
			.send();
	}
});

export default router;
