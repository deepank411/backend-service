import findAndUpdateUser from '../user/v1.findAndUpdateUser';
import createUser from '../user/v1.createUser';
import { createTokens } from '../../utils/auth';

export const login = async (params) => {
	const { fbUserId, googleUserId, email } = params;

	let userData = await findAndUpdateUser({
		...fbUserId && { fbUserId },
		...googleUserId && { googleUserId },
		...email && { email },
	});

	if (!userData) {
		if (!(googleUserId || fbUserId)) {
			throw new Error('ClientId and Provider is required to create an account');
		}
		userData = await createUser(params);
	}

	console.log('userData', userData);

	const modifiedParams = {
		id: userData.id,
		...params,
	};

	const { accessToken } = createTokens(modifiedParams);

	return {
		user: userData,
		accessToken,
	};
};
