import { Router } from 'express';

import { login } from './v1.auth';

const router = new Router();

router.route('/api/v1/login').post(async (req, res) => {
	try {
		console.log('here in login');
		const data = await login(req.body);
		console.log('dd', data);
		res.create(data)
			.success()
			.send();
	} catch (e) {
		console.log('e', e);
		res.create('Error')
			.badrequest()
			.send();
	}
});

export default router;
