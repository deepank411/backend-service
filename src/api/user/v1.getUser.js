import { findQuery, findOneQuery } from '../../helpers/generalQueries';
import UserModel from '../../models/user.model';
import { cleanMongoObject, isNotEmptyObject } from '../../helpers/commonHelpers';

const getUser = async (params) => {
	const data = await findOneQuery(UserModel, params);
	if (isNotEmptyObject(data)) {
		return cleanMongoObject(data.toObject());
	}
};

export default getUser;
