/* eslint-disable import/prefer-default-export */
import UserModel from '../../models/user.model';
import { cleanMongoObject } from '../../helpers/commonHelpers';
import { saveQuery } from '../../helpers/generalQueries';

const createUser = async (params) => {
	const user = new UserModel({
		...params,
		coverPicture: params.coverPicture,
	});
	const newUser = await saveQuery(user);
	if (!newUser) {
		throw new Error('could not create save user');
	}
	return cleanMongoObject(newUser.toObject());
};

export default createUser;
