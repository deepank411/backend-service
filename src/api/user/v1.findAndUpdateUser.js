import { findQuery, findOneQuery, findOneAndUpdateQuery } from '../../helpers/generalQueries';
import UserModel from '../../models/user.model';
import { cleanMongoObject, isNotEmptyObject } from '../../helpers/commonHelpers';

const findAndUpdateUser = async (params) => {
	console.log('findAndUpdateUser params', params);
	const modifiedParams = {
		$or: [
			...params.email ? [ { email: params.email } ] : [],
			...params.fbUserId ? [ { fbUserId: params.fbUserId } ] : [],
			...params.googleUserId ? [ { googleUserId: params.googleUserId } ] : [],
		],
	}
	const data = await findOneAndUpdateQuery(UserModel, modifiedParams, params, { new: true });
	if (isNotEmptyObject(data)) {
		return cleanMongoObject(data.toObject());
	}
};

export default findAndUpdateUser;
