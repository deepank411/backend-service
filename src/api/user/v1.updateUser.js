import { findOneAndUpdateQuery } from '../../helpers/generalQueries';
import UserModel from '../../models/user.model';
import { isNotEmptyObject } from '../../helpers/commonHelpers';

const updateUser = async (params) => {
	const dd = await findOneAndUpdateQuery(
		UserModel,
		{ _id: params.userId },
		{
			...(isNotEmptyObject(params.setParams) && { $set: params.setParams }),
			...(isNotEmptyObject(params.unsetParams) && { $unset: params.unsetParams }),
		},
		{ new: true },
	);
	return dd;
};

export default updateUser;
