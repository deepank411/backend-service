import { Router } from 'express';

import updateUser from './v1.updateUser';

const router = new Router();

router.route('/v1/user').put(async (req, res) => {
	try {
		const dd = await updateUser({
			userId: req.user.id,
			setParams: {
				...(req.body.gender && { gender: req.body.gender }),
				...(req.body.dob && { dateOfBirth: req.body.dob }),
				...(req.body.genderPreference && { genderPreference: req.body.genderPreference }),
			},
		});
		res.create(dd)
			.success()
			.send();
	} catch (e) {
		console.log('e', e);
		res.create(e)
			.badrequest()
			.send();
	}
});

export default router;
