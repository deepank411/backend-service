import URL from 'url';
import { verify } from 'jsonwebtoken';
import getUser from '../api/user/v1.getUser';

const allowedPaths = ['/', '/api/v1/login', '/api/v1/create-new-mongo-id', '/graphql', '/api/ping'];

const handleNoAuthToken = (req, res, next) => {
	const url = URL.parse(req.originalUrl || req.url, true);

	const isAllowedPath = allowedPaths.filter((path) => (path instanceof RegExp && !!path.exec(url.pathname)) || path === url.pathname).length > 0;

	return isAllowedPath ? next() : res.status(401).send('UnauthorizedError');
};

const checkJWT = async (req, res, next) => {
	console.log('req.headers', req.headers);
	const { authorization } = req.headers;

	if (!authorization) {
		return handleNoAuthToken(req, res, next);
	}

	try {
		const accessToken = authorization.split(' ')[1];
		const verifiedToken = verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
		const user = await getUser({ _id: verifiedToken.id });
		Object.assign(req, { user });

		return next();
	} catch (e) {
		return handleNoAuthToken(req, res, next);
	}
};

export default checkJWT;
