import winston from 'winston';
import 'winston-daily-rotate-file';

const logger = winston.createLogger({
	level: 'info',
	format: winston.format.json(),
});

if (process.env.NODE_ENV === 'development') {
	logger.add(
		new winston.transports.Console({
			format: winston.format.simple(),
		}),
	);
}

if (process.env.NODE_ENV === 'production') {
	logger
		.add(
			new winston.transports.DailyRotateFile({
				filename: 'log/error-%DATE%.log',
				datePattern: 'YYYY-MM-DD-HH',
				zippedArchive: true,
				maxSize: '20m',
				level: 'error',
				maxFiles: '14d',
			}),
		)
		.add(
			new winston.transports.DailyRotateFile({
				filename: 'log/info-%DATE%.log',
				datePattern: 'YYYY-MM-DD-HH',
				zippedArchive: true,
				maxSize: '20m',
				level: 'info',
				maxFiles: '14d',
			}),
		);
}

export default logger;
