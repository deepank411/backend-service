import { verify } from 'jsonwebtoken';
import { getUser } from '../api/user/v1.user';

export const setUserFromJwt = async (req, res, next) => {
	if (req.headers.source === 'ANDROID' || req.headers.source === 'IOS') {
		const accessToken = req.headers['access-token'];
		if (!accessToken) {
			return next();
		}
		try {
			const data = verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
			const user = await getUser({
				id: data.id,
				provider: data.provider,
			});

			Object.assign(req, { user });
			return next();
		} catch (e) {
			// console.log('e', e);
		}
		next();
	} else {
		return next();
	}
};
