const RESTInterceptor = ({ method, url, body }) =>
	fetch(url, {
		method,
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(body),
	})
		.then((x) => (x.status !== 204 ? x.json() : { status: 204 }))
		.catch((e) => {
			throw new Error(e);
		});

export default RESTInterceptor;
