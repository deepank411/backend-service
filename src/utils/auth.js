import { sign } from 'jsonwebtoken';

export const createTokens = (data) => {
	const accessToken = sign(data, process.env.ACCESS_TOKEN_SECRET, {
		expiresIn: '30d',
	});
	return { accessToken };
};
