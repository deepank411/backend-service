import aws from 'aws-sdk';

export default new aws.S3({
	accessKeyId: process.env.AWS_KEY,
	secretAccessKey: process.env.AWS_SECRET,
	region: process.env.AWS_REGION,
	s3ForcePathStyle: true,
});
