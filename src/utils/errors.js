import logger from './filelogger';
import { accessDeepObject } from '../helpers/commonHelpers';

export const LogResolver = (err, req, res, next) => {
	logger.log({
		level: 'error',
		id: res.reqid,
		error: JSON.stringify(err),
		message: err.message,
	});
	next(err);
};

export class GraphqlQueryError extends Error {
	constructor(data) {
		super();
		this.message = 'Graphql Query Failed';
		this.data = data;
	}
}

export const GraphqlQueryErrorResolver = (err, req, res, next) => {
	if (err instanceof GraphqlQueryError) {
		res.create()
			.badrequest('Graphql Query Failed', 400, accessDeepObject('data.errors', err))
			.send();
		return;
	}
	next(err);
};

export class GraphqlConnectionError extends Error {
	constructor(msg, errors) {
		super();
		this.message = msg || 'Unable to connect to graphql Server';
		this.errors = errors;
	}
}

export class SupressedError extends Error {
	constructor(msg, errors) {
		super();
		this.message = msg || 'Error Supressed and 200 Sent';
		this.errors = errors;
	}
}

export class NoDataError extends Error {}

export const NoDataErrorResolver = (err, req, res, next) => {
	if (err instanceof NoDataError) {
		res.create()
			.nodata()
			.send();
		return;
	}
	next(err);
};

export const GraphqlConnectionErrorResolver = (err, req, res, next) => {
	if (err instanceof GraphqlConnectionError) {
		res.create()
			.badrequest('Graphql Connection Error', 400, err.errors)
			.send();
		return;
	}
	next(err);
};

export const SupressedErrorResolver = (err, req, res, next) => {
	if (err instanceof SupressedError) {
		res.create(err.errors)
			.success('Supressed Error:' + err.message, 200)
			.send();
		return;
	}
	next(err);
};

export class ForbiddenError extends Error {
	constructor(msg, errors) {
		super();
		this.message = msg || 'Forbidden';
		this.errors = errors;
	}
}

export class MessageError extends Error {
	constructor(errors) {
		super();
		// this.message = msg;
		this.errors = errors;
	}
}

export const MessageErrorResolver = (err, req, res, next) => {
	if (err instanceof MessageError) {
		res.create(err.errors)
			.badrequest(err.errors.first(), 400)
			.send();
		return;
	}
	next(err);
};

export const ForbiddenErrorResolver = (err, req, res, next) => {
	if (err instanceof ForbiddenError) {
		res.create(err.errors)
			.forbidden('Forbidden Error:' + err.message, 403)
			.send();
		return;
	}
	next(err);
};

export class UnauthorisedError extends Error {
	constructor(msg, errors) {
		super();
		this.message = msg || 'Unauthorised';
		this.errors = errors;
	}
}

export class UnprocessableEntityError extends Error {
	// Has no message just array of errors
	constructor(errors) {
		super();
		this.errors = errors;
	}
}

export const UnauthorisedErrorResolver = (err, req, res, next) => {
	if (err instanceof UnauthorisedError) {
		res.create(err.errors)
			.unauthorised('Unauthorised Error:' + err.message, 401)
			.send();
		return;
	}
	next(err);
};

export const UnprocessableEntityErrorResolver = (err, req, res, next) => {
	if (err instanceof UnauthorisedError) {
		res.create(err.errors)
			.unprocessable(undefined, undefined, err.errors)
			.send();
		return;
	}
	next(err);
};

const ignoreTokensInThesePaths = [];

export const TokenResolver = (req, res, next) => {
	if (ignoreTokensInThesePaths.includes(req.path)) {
		next();
		return;
	}
	if (!req.headers.token) {
		throw new UnauthorisedError('No token received ' + req.path);
	}
	next();
};
