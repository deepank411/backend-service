import redis from 'redis';

const redisConnection = () => {
	const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);

	client.on('connect', () => console.log('Redis client connected'));

	client.on('error', (err) => console.log('Something went wrong with redis: ' + err));

	return client;
};

export default redisConnection;
