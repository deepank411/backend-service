import { Router } from 'express';

const router = new Router();

router.route('/').get((req, res) => {
	res.create('server working fine')
		.success()
		.send();
});

router.route('/api/ping').get((req, res) => {
	res.create('pong')
		.success()
		.send();
});

export default router;
