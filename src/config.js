import path from 'path';

const config = {
	port: 8080,
	logFileDir: path.join(__dirname, '../log'),
	logFileName: 'app.log',
};

export default config;
