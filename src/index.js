/* eslint-disable function-paren-newline */
import Express from 'express';
import http from 'http';
import path from 'path';
import compression from 'compression';
import bodyParser from 'body-parser';
import cors from 'cors';
import { ApolloServer, PubSub } from 'apollo-server-express';
import { fileLoader, mergeTypes } from 'merge-graphql-schemas';

import fileLogger from './utils/filelogger';
import dbConnection from './utils/mongoose';

import upgradeResponse from './utils/responseconstuctor';

import serverConfig from './config';
import checkJWT from './utils/jwt';

import resolvers from './resolvers';
import {
	GraphqlConnectionErrorResolver,
	GraphqlQueryErrorResolver,
	SupressedErrorResolver,
	UnauthorisedErrorResolver,
	ForbiddenErrorResolver,
	NoDataErrorResolver,
	MessageErrorResolver,
	LogResolver,
} from './utils/errors';

import routes from './routes';
import authRoutes from './api/auth/routes';
import miscRoutes from './api/misc/routes';

const app = new Express(); // Initialize the Express App

// Set the ip-address of your trusted reverse proxy server such as
// haproxy or Apache mod proxy or nginx configured as proxy or others.
// The proxy server should insert the ip address of the remote client
// through request header 'X-Forwarded-For' as
// 'X-Forwarded-For: some.client.ip.address'
// Insertion of the forward header is an option on most proxy software
app.set('trust proxy', true);

const whitelist = [process.env.BACKEND_SERVICE_ENDPOINT];
const corsOptions = {
	credentials: true,
	origin: (origin, cb) => {
		if (whitelist.indexOf(origin) !== -1 || !origin) {
			cb(null, true);
		} else {
			cb(new Error(`Not allowed by CORS. Origin - ${origin}`));
		}
	},
};
app.use(cors(corsOptions));
app.use(compression());
// app.use(cookieParser());
app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: false }));
app.use(LogResolver, GraphqlQueryErrorResolver, GraphqlConnectionErrorResolver, SupressedErrorResolver, UnauthorisedErrorResolver, ForbiddenErrorResolver, NoDataErrorResolver, MessageErrorResolver);

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', req.headers.origin);
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});

fileLogger.info('Yay! app is running');

dbConnection();

upgradeResponse(app).use(checkJWT); // use JWT auth to secure the api

// route handlers
upgradeResponse(app).use(routes);
upgradeResponse(app).use(authRoutes);
upgradeResponse(app).use(miscRoutes);

const typeDefs = mergeTypes(fileLoader(path.join(__dirname, './schema')));

const pubsub = new PubSub();

const apolloServer = new ApolloServer({
	typeDefs,
	resolvers,
	playground: {
		settings: {
			'request.credentials': 'include',
		},
	},
	context: ({ req, res, connection }) => {
		// console.log('in context', req.user);
		const ctx = {
			req,
			res,
			// redisClient,
			user: (req || {}).user,
			logout: () => req.logout(),
			pubsub,
		};
		if (connection) {
			// console.log('connection', connection.context);
			return {
				...ctx,
				...connection.context,
			};
		}
		// console.log('ctx', ctx, connection);
		return ctx;
	},
	introspection: true,
	subscriptions: {
		path: '/subscriptions',
		onConnect: (connectionParams, webSocket, context) => {
			console.log('WebSocket Connected');
			// console.log('on connect', context);
		},
		onDisconnect: () => {
			console.log('WebSocket Disconnected');
		},
	},
});

apolloServer.applyMiddleware({ app, cors: false });

const httpServer = http.createServer(app);
apolloServer.installSubscriptionHandlers(httpServer);

// ⚠️ Pay attention to the fact that we are calling `listen` on the http server variable, and not on `app`.
httpServer.listen(serverConfig.port, () => {
	console.log(`🚀 Server ready at http://localhost:${serverConfig.port}${apolloServer.graphqlPath}`);
	console.log(`🚀 Subscriptions ready at ws://localhost:${serverConfig.port}${apolloServer.subscriptionsPath}`);
});

// start app
// app.listen(serverConfig.port, (error) => {	apolloServer.installSubscriptionHandlers(httpServer);
// 	if (error) {
// 		console.log('Something Went Wrong');
// 		return;
// 	}
// 	console.log(`Server running at ${serverConfig.port}`);
// });

// export default app;
