import { requiresAuth } from "../utils/permissions";

export default {
	Query: {
		me: async (_, { params = {} }, { user }) => {
			return user;
		},
		test: async (_, { params }) => {
			return true;
		},
		authenticatedEndpoint: requiresAuth.createResolver(async (_, { params }, { user }, info) => {
			console.log('user', user);
			return true;
		}),
	},
};
