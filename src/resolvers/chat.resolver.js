import { withFilter } from 'apollo-server-express';

import { getGraphqlFieldMap, omitWrapper, ArrayMaybe } from '../helpers/commonHelpers';
import { requiresAuth } from '../utils/permissions';
import getMessageListing from '../api/chat/v1.getMessageListing';
import getConversations from '../api/chat/v1.getConversations';
import starMessage from '../api/chat/v1.starMessage';
import deleteMessage from '../api/chat/v1.deleteMessage';
import updateMessage from '../api/chat/v1.updateMessage';
import createMessage from '../api/chat/v1.createMessage';
import readMessage from '../api/chat/v1.readMessage';
import archiveConversation from '../api/chat/v1.archiveConversation';
import updateConversation from '../api/chat/v1.updateConversation';
import getConversation from '../api/chat/v1.getConversation';

const CONVERSATION_UPDATED = 'CONVERSATION_UPDATED';
const NEW_MESSAGE_RECEIVED = 'NEW_MESSAGE_RECEIVED';
const MESSAGE_UPDATED = 'MESSAGE_UPDATED';
const MESSAGE_EDITED = 'MESSAGE_EDITED';
const MESSAGE_DELETED = 'MESSAGE_DELETED';

export default {
	Subscription: {
		newMessageReceived: {
			subscribe: withFilter(
				(_, __, { pubsub }) => pubsub.asyncIterator([NEW_MESSAGE_RECEIVED]),
				(payload, { params }) => {
					if (payload.msgTo.includes(params.userId)) {
						return true;
					}
					return false;
				},
			),
		},
		conversationUpdated: {
			subscribe: withFilter(
				(_, __, { pubsub }) => pubsub.asyncIterator([CONVERSATION_UPDATED]),
				(payload, { params }) => {
					if (payload.userId === params.userId) {
						return true;
					}
					return false;
				},
			),
		},
		messageUpdated: {
			subscribe: withFilter(
				(_, __, { pubsub }) => pubsub.asyncIterator([MESSAGE_UPDATED]),
				(payload, { params }) => {
					if (payload.messageUpdated.conversationId === params.conversationId) {
						return true;
					}
					return false;
				},
			),
		},
		messageEdited: {
			subscribe: withFilter(
				(_, __, { pubsub }) => pubsub.asyncIterator([MESSAGE_EDITED]),
				(payload, { params }) => {
					if (payload.messageEdited.conversationId === params.conversationId) {
						return true;
					}
					return false;
				},
			),
		},
		messageDeleted: {
			subscribe: withFilter(
				(_, __, { pubsub }) => pubsub.asyncIterator([MESSAGE_DELETED]),
				(payload, { params }) => {
					if (payload.messageDeleted.conversationId === params.conversationId) {
						return true;
					}
					return false;
				},
			),
		},
	},
	Query: {
		chatMessages: requiresAuth.createResolver(async (_, { params }, { user }) =>
			getMessageListing({
				operator: 'lt',
				...params,
				userId: user.id,
			}),
		),
		chatConversations: requiresAuth.createResolver(async (_, { params }, { user }, info) => {
			const gqlFields = getGraphqlFieldMap(info.fieldNodes[0].selectionSet);
			const withUserDetails = gqlFields.findIndex((x) => x.fieldName === 'userDetails') !== -1;
			return getConversations({
				withUserDetails,
				userId: user.id,
				onlyArchived: params.onlyArchived,
			});
		}),
		getConversation: requiresAuth.createResolver(async (_, { params }, { user }, info) =>
			getConversation({
				...params,
				userId: user.id,
			}),
		),
	},
	Mutation: {
		archiveConversation: requiresAuth.createResolver(async (parent, { params }, { pubsub, user }, info) => {
			const gqlFields = getGraphqlFieldMap(info.fieldNodes[0].selectionSet);
			const withUserDetails = gqlFields.findIndex((x) => x.fieldName === 'userDetails') !== -1;
			const conv = await archiveConversation({
				...params,
				id: params.id,
				userId: user.id,
				withUserDetails,
			});

			pubsub.publish(CONVERSATION_UPDATED, { conversationUpdated: { ...conv, eventType: params.isArchived ? 'CONVERSATION_ADDED' : 'CONVERSATION_ARCHIVED' }, userId: user.id });

			return conv;
		}),
		updateConversation: requiresAuth.createResolver(async (parent, { params }, { pubsub, user }) => {
			const conv = await updateConversation({
				...params,
				userId: user.id,
			});

			// Notifiy all newly added users about the conversation
			ArrayMaybe(params.usersToAdd).forEach((userId) => {
				pubsub.publish(CONVERSATION_UPDATED, { conversationUpdated: { ...conv, eventType: 'CONVERSATION_ADDED' }, userId });
			});
			return conv;
		}),
		addMessage: requiresAuth.createResolver(async (parent, { params }, { pubsub, user }) => {
			const userId = params.userId || user.id;
			const senderName = params.senderName || user.fullName;
			const dd = await createMessage({
				...omitWrapper(['clientId', 'senderName'], { ...params }),
				userId,
			});

			if (dd.isNewConversation) {
				params.msgTo.forEach((userId) => pubsub.publish(CONVERSATION_UPDATED, { conversationUpdated: { ...dd.conversation, eventType: 'CONVERSATION_ADDED' }, userId }));
				pubsub.publish(CONVERSATION_UPDATED, { conversationUpdated: { ...dd.conversation, unreadCount: 0, eventType: 'CONVERSATION_ADDED' }, userId }); // for the sender we have to make the unreadCount 0
				pubsub.publish(NEW_MESSAGE_RECEIVED, { newMessageReceived: dd.message, msgTo: params.msgTo });
			} else {
				pubsub.publish(NEW_MESSAGE_RECEIVED, { newMessageReceived: dd.message, msgTo: params.msgTo });
				pubsub.publish(MESSAGE_UPDATED, { messageUpdated: { ...dd.message, eventType: 'MESSAGE_ADDED', clientId: params.clientId }, user, msgTo: params.msgTo });
			}
			// else if (dd.shouldRecipientGetNotified) {
			// pubsub.publish(CONVERSATION_UPDATED, { conversationUpdated: { ...dd.conversation, eventType: 'CONVERSATION_ADDED' }, userId: params.msgTo[0] });
			// }
			return dd.message;
		}),
		editMessage: requiresAuth.createResolver(async (parent, { params }, { pubsub, user }) => {
			const dd = await updateMessage({
				...params,
				userId: user.id,
			});
			pubsub.publish(MESSAGE_EDITED, { messageEdited: dd, user });
			return dd;
		}),
		deleteMessage: requiresAuth.createResolver(async (parent, { params }, { pubsub, user }) => {
			const dd = await deleteMessage({
				...params,
				userId: user.id,
			});
			pubsub.publish(MESSAGE_UPDATED, { messageUpdated: { ...dd, isLatestMessage: !!params.isLatestMessage, eventType: 'MESSAGE_DELETED' }, user, msgTo: params.msgTo });
			return dd;
		}),
		starMessage: requiresAuth.createResolver(async (parent, { params }, { user }) => {
			const dd = await starMessage({
				...params,
				userId: user.id,
			});
			return dd;
		}),
		readMessages: requiresAuth.createResolver(async (parent, { params }, { pubsub, user }) => {
			const dd = await readMessage({
				...params,
				userId: user.id,
			});

			dd.forEach((msg) => {
				pubsub.publish(MESSAGE_UPDATED, { messageUpdated: { ...msg, eventType: 'MESSAGE_READ' } });
			});

			return dd;
		}),
	},
};
