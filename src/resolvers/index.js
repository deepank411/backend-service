import { mergeResolvers } from 'merge-graphql-schemas';

import chatResolver from './chat.resolver';
import userResolver from './user.resolver';

export default mergeResolvers([
	chatResolver,
	userResolver,
]);
