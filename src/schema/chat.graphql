type Subscription {
	newMessageReceived(params: UserIdInput): Message
	conversationUpdated(params: UserIdInput): ConversationUpdatedOutput
	messageUpdated(params: MessageCRUDInput): MessageUpdatedOutput
	messageEdited(params: MessageCRUDInput): Message
	messageDeleted(params: MessageCRUDInput): Message
}

type Query {
	chatMessages(params: MessagesInput): [Message]
	chatConversations(params: ConversationsInput): [Conversation]
	getConversation(params: ConversationInput): Conversation
}

type Mutation {
	archiveConversation(params: ArchiveConversationInput): Conversation
	updateConversation(params: UpdateConversationInput): Conversation
	addMessage(params: CreateMessageInput): AddMessageOutput
	editMessage(params: EditMessageInput): Message
	deleteMessage(params: DeleteMessageInput): Message
	starMessage(params: StarMessageInput): Message
	readMessages(params: ReadMessageInput): [Message]
}

input MessageCRUDInput {
	userId: ID
	conversationId: ID
}

input ConversationsInput {
	userId: ID
	onlyArchived: Boolean
	operator: String
	sortKey: String
	sortOrder: String
	limit: Int
}

input MessagesInput {
	userId: ID
	conversationId: ID
	lastMessageId: ID
	operator: String
	sortKey: String
	sortOrder: String
	limit: Int
}

input CreateMessageInput {
	msgType: String!
	msg: String!
	conversationId: ID
	conversationName: String
	msgTo: [ID!]!
	clientId: String
	userId: ID
	senderName: String
}

input EditMessageInput {
	id: ID!
	msg: String!
	oldMsg: String!
	isLatestMessage: Boolean
	conversationId: ID!
	userId: ID!
}

input DeleteMessageInput {
	id: ID!
	isLatestMessage: Boolean
	conversationId: ID!
	userId: ID!
}

input StarMessageInput {
	id: ID!
	userId: ID!
	isMessageStarred: Boolean!
}

input ReadMessageInput {
	msgIds: [ID!]!
	userId: ID!
	conversationId: ID!
}

type Message {
	id: ID
	userId: ID
	msgType: String
	msg: String
	conversationId: ID
	status: Int
	readBy: [UserId]
	isDeleted: Int
	isEdited: Int
	starredBy: [UserId]
	editHistory: [EditHistory]
	createdAt: String
}

type EditHistory {
	msg: String
	editedAt: String
}

input ConversationInput {
	msgTo: [ID!]!
	userId: ID!
}

input ArchiveConversationInput {
	id: ID!
	isArchived: Boolean!
}

input UpdateConversationInput {
	conversationId: ID!
	conversationName: String
	usersToAdd: [ID]
	usersToRemove: [ID]
}

type Conversation {
	id: ID
	users: [UserId]
	lastMessageId: ID
	lastMessageText: String
	lastMessageType: String
	lastMessageTime: String
	lastMessageFrom: ID
	userDetails: [User]
	createdBy: ID
	createdAt: String
	unreadCount: Int
	conversationName: String
}

enum chatEventTypes {
	CONVERSATION_ADDED
	CONVERSATION_ARCHIVED
	MESSAGE_ADDED
	MESSAGE_EDITED
	MESSAGE_DELETED
	MESSAGE_READ
}

type ConversationUpdatedOutput {
	id: ID
	users: [UserId]
	lastMessageId: ID
	lastMessageText: String
	lastMessageType: String
	lastMessageTime: String
	lastMessageFrom: ID
	userDetails: [User]
	createdAt: String
	eventType: chatEventTypes
	unreadCount: Int
	conversationName: String
}

type MessageUpdatedOutput {
	id: ID
	userId: ID
	msgType: String
	msg: String
	conversationId: ID
	status: Int
	isDeleted: Int
	isEdited: Int
	readBy: [UserId]
	starredBy: [UserId]
	editHistory: [EditHistory]
	createdAt: String
	eventType: chatEventTypes
	clientId: String
	isLatestMessage: Boolean
}

type AddMessageOutput {
	id: ID
	userId: ID
	msgType: String
	msg: String
	conversationId: ID
	status: Int
	isDeleted: Int
	isEdited: Int
	starredBy: [ID]
	editHistory: [EditHistory]
	createdAt: String
	clientId: String
}
