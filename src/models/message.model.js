import { Schema, model } from 'mongoose';

const messageTypeEnum = ['TEXT', 'IMAGE'];
const MESSAGE_STATUS_SENT = 1;

const editHistorySchema = new Schema(
	{
		msg: String,
		editedAt: Date,
	},
	{ _id: false },
);

const userSchema = new Schema(
	{
		userId: Schema.Types.ObjectId,
		createdAt: {
			type: Date,
			default: Date.now(),
		},
	},
	{ _id: false },
);

const messageSchema = new Schema(
	{
		msg: {
			type: String,
		},
		msgType: {
			type: String,
			enum: messageTypeEnum,
			default: 'TEXT',
		},
		conversationId: {
			type: Schema.Types.ObjectId,
			default: null,
		},
		userId: {
			type: Schema.Types.ObjectId,
			default: null,
		},
		status: { type: Number, default: MESSAGE_STATUS_SENT },
		isEdited: {
			type: Number,
			default: 0,
		},
		isDeleted: {
			type: Number,
			default: 0,
		},
		starredBy: [userSchema],
		readBy: [userSchema],
		editHistory: [editHistorySchema],
	},
	{
		timestamps: true,
	},
);

const Model = model('Message', messageSchema);
export default Model;
