import { Schema, model } from 'mongoose';
import { STATUS_ACTIVE, STATUS_INACTIVE, STATUS_VERIFIED, STATUS_BLOCKED, STATUS_UNVERIFIED } from '../utils/constants';

const userSchema = new Schema(
	{
		fbUserId: String,
		googleUserId: String,
		email: String,
		fullName: String,
		coverPicture: String,
		status: {
			type: Number,
			enum: [STATUS_ACTIVE, STATUS_INACTIVE, STATUS_VERIFIED, STATUS_BLOCKED, STATUS_UNVERIFIED],
			default: STATUS_ACTIVE,
		},
	},
	{ timestamps: true },
);

const UserModel = model('User', userSchema);
export default UserModel;
