import { Schema } from 'mongoose';

const photoSchema = new Schema(
	{
		id: {
			type: Schema.Types.ObjectId,
			ref: 'Image',
		},
		url: {
			type: String,
		},
	},
	{ _id: false },
);

export default photoSchema;
