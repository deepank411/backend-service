import { Schema, model } from 'mongoose';

const userSchema = new Schema(
	{
		userId: Schema.Types.ObjectId,
		unreadMsgCount: Number,
	},
	{ _id: false },
);

const messageTypeEnum = ['TEXT', 'IMAGE'];

const conversationSchema = new Schema(
	{
		conversationName: String,
		users: [userSchema],
		lastMessageId: Schema.Types.ObjectId,
		lastMessageText: String,
		lastMessageTime: Date,
		lastMessageFrom: Schema.Types.ObjectId,
		lastMessageType: {
			type: String,
			enum: messageTypeEnum,
			default: 'TEXT',
		},
		isArchivedBy: [userSchema],
		createdBy: Schema.Types.ObjectId,
	},
	{
		timestamps: true,
	},
);

const Model = model('Conversation', conversationSchema);
export default Model;
