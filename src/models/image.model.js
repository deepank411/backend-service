import { Schema, model } from 'mongoose';
import { STATUS_ACTIVE } from '../utils/constants';

const imageSchema = new Schema(
	{
		url: {
			type: String,
		},
		entityType: {
			type: String, // USER/STORY
		},
		entityId: {
			type: Schema.Types.ObjectId,
		},
		status: {
			type: Number,
			default: STATUS_ACTIVE,
		},
		createdBy: {
			type: Schema.Types.ObjectId,
			ref: 'User',
		},
	},
	{ timestamps: true },
);

const Model = model('Image', imageSchema);
export default Model;
