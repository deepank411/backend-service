import queryString from 'query-string';
import { filter, path, pick, omit } from 'ramda';
import reduce from 'lodash/reduce';

export const ArrayMaybe = (arr) => arr || [];
export const ObjectMaybe = (obj) => obj || {};
export const StringMaybe = (str) => str || '';
export const renameObjectKeys = (obj, newKeysmap) =>
	reduce(
		obj,
		(result, value, key) => {
			key = newKeysmap[key] || key;
			result[key] = value;
			return result;
		},
		{},
	);
export const pickWrapper = (keys, object) => pick(keys, object);
export const omitWrapper = (keys, object) => omit(keys, object);
export const isNotEmptyArray = (x) => x && x.length > 0;
export const isNotEmptyObject = (obj) => obj && Object.keys(obj).length > 0;
export const removeNonTrueValuesFromObject = (obj) => filter(Boolean, obj);
export const accessDeepObject = (arr, obj) => path(Array.isArray(arr) ? arr : arr.split('.'), obj);

export const stringifyQueryParams = (qp, url = '') => {
	if (!isNotEmptyObject(qp)) return '';
	return url.includes('?') ? `&${queryString.stringify(qp)}` : `?${queryString.stringify(qp)}`;
};

export const getGraphqlFieldMap = (selectionSet) => {
	const map = [];
	selectionSet.selections.map((x) => {
		map.push({
			fieldName: x.name.value,
			...(x.selectionSet &&
				x.selectionSet.selections && {
					children: getGraphqlFieldMap(x.selectionSet),
				}),
		});
	});
	return map;
};

export const cleanMongoObject = (obj, customKeyName = 'id') =>
	omitWrapper(
		['__v'],
		renameObjectKeys(obj, {
			_id: customKeyName,
		}),
	);

export const nowMongoDate = () => new Date().toISOString();

export const numberRange = (start, end) =>
	// eslint-disable-next-line no-mixed-operators
	Array(end - start + 1)
		.fill()
		.map((_, idx) => start + idx);

export const dateToMongoDate = (input) => (input instanceof Date ? input.toISOString() : input);
